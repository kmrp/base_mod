package ru.konungstvo.basemod;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import static net.minecraftforge.common.MinecraftForge.EVENT_BUS;

// In Forge mods, this is usually called CommonProxy
public class ServerProxy {

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
    }

    @Mod.EventHandler
    public void startServer(FMLServerStartingEvent event) {
        System.out.println(Core.NAME + " is starting!");
        EVENT_BUS.register(this);

    }
}
