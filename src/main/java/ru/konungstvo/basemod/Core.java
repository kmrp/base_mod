package ru.konungstvo.basemod;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = Core.MODID, name = Core.NAME, version = Core.VERSION)
public class Core {
    public static final String MODID = "basemod";
    public static final String NAME = "Base Mod";
    public static final String VERSION = "0.1";

    private static Logger logger;

    @SidedProxy(
            clientSide = "ru.konungstvo.basemod.ClientProxy",
            serverSide = "ru.konungstvo.basemod.ServerProxy"
    )
    protected static ServerProxy proxy;

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void onServerStart(FMLServerStartingEvent event) {
        proxy.startServer(event);

    }
}